**Below are the Generic Errors occur during Integration, please cross-check if you have made below configurations:**

### 1. Something went wrong:Value<!DOCTYPE of type java.lang.String cannot be converted into json object
[Watch the Error Screenshot here](https://drive.google.com/file/d/0B5H1Jbm8ax4ebGxTbmxLWmNOcHM/view).

This error occurs when merchant uses any of the following incorrectly.    
a) Wrong Key  
b) Wrong Salt  
c) Wrong Environment

**Solution** : Please make sure the test key being used if you are hitting the test environment and vice-versa.

 > Note: Please use test card if you are hitting test environment and vice-versa.

###2. Mandatory Param Hash is missing
[Watch the Error Screenshot here](https://drive.google.com/file/d/0B5H1Jbm8ax4eQ09TcTFIQTdCb2s/view?usp=sharing).

This error comes when merchant is not generating the **PaymentRelatedDetailsForMobileSDK**  
 hash from their server and not being passed to call **PayU’s Webservice**.   

Below is the formula to generate this hash:
 **sha512(key|command|var1|salt)**      

Key - **Merchant key/Test key**  
Command - **payment_related_details_for_mobile_sdk**  
Var1 - **default/user_credentials**  
Salt - **Merchant salt/Test salt**  


###3. Incorrectly Calculated Hash Parameter
[Watch the Error Screenshot here](https://drive.google.com/drive/folders/0B5H1Jbm8ax4eY0xyZThrQy04Nms)

This error comes when merchant is not calculating the **Payment Hash** correctly or the parameters used in generating the payment hash are different from the parameters being sent to PayU’s Server for Payment Request. 
	
Formula for calculating payment hash :
**key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt**

Please go through [the Server Side Documentation](https://github.com/payu-intrepos/Documentations/wiki/4.-Server-Side) for your reference.

Moreover, you can validate your hashes from our [online tool from here](https://payuhashgeneration.herokuapp.com/).


###4. Redirection issue from Surl-Furl (Success/Failure URLs)

You need to deploy **surl-furl** pages on your server and you need to send the same surl-furl in post params while making payment request.

After completion of transaction (success/failure), PayU will redirects post the transaction data to your surl-furl. [Please refer Server Side Documentation](https://github.com/payu-intrepos/Documentations/wiki/4.-Server-Side) for more details.


###5. Invalid Hash 

This error occurs when merchant is not generating the **PaymentRelatedDetailsForMobileSDK** hash or **VASForMobileSDK** hash correctly. 


The formula for generating the PaymentRelatedDetailsForMobileSDK or VASForMobileSDK hash is:
   **sha512(key|command|var1|salt)**

   Key - **Merchantkey/Testkey**  
   Command - **payment_related_details_for_mobile_sdk**  or **vas_for_mobile_sdk**   
   Var1 - **default/user_credentials**  
   Salt - **MerchantSalt/TestSalt**  


###6. Merchant is not authorized to use the store card API 

This toast comes when the store card flag is not enabled on the merchant key.
For this, you need to contact the Key Account Manager at PayU to enable the store card flag on merchant key.


####Please write to us at **mobile-integration@payu.in** for further details.









