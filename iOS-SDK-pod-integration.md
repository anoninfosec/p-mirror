* open terminal and install cocoapod

          sudo gem install cocoapods

* Once installation is done, you need to set up command 

          pod setup

and all done.

* Create new Project in X-Code

* Go inside the directory and give the location 

          e.g.  ls  ->  cd Desktop/YourProjectName


* Now we are going to make an actual pod file

Type in terminal:

          nano Podfile

It will open an Editor, in Editor type :

          platform :ios, '8.0'
          use_frameworks!  
          target 'YourProjectName'  do
          pod 'PayUIndia-PG-SDK', '~> 3.8'

          end     


Now, press ctlr-x to leave the editor.

* Last thing we have to do is to type in terminal

          pod install
* Now, go to your project folder and open
          TestCocoaProject.xcworkspace

* Inside, Pods directory, you’ll find PayUIndia-PG-SDK

* To integrate further, please follow [integration docs from here](https://github.com/payu-intrepos/Documentations/wiki/8.-iOS-SDK-integration#seamless).
