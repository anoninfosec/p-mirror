#### Question 1 : How to switch environment from testing to production in PayU SDK?
Ans : Please refer sample app.Please change value of env variable accordingly for testing or production, in MainActivity in sample app. 

Please refer following code :

    int env = PayuConstants.PRODUCTION_ENV; //for production
    int env = PayuConstants.STAGING_ENV; //for testing





#### Question 2 : Why do we need so many hashes?
Ans : For security purpose, hash is mandatory.Whenever you talk to PayU server you need a hash.For every API there is a separate hash because all API are public.

#### Question 3 : Do we need to calculate Reverse Hash?
Ans : It is MANDATORY to calculate reverse hash on your surl/furl to validate a transaction at your end. Otherwise, the transaction maybe hacked or tempered with.

For more info, please refer [Server Side Document](https://github.com/payu-intrepos/Documentations/wiki/4.-Server-Side) for more details.

#### Question 4 : What are mandatory parameters for making payment?
Ans : Please refer page no. 8 in [Integration Document ver2.5 ](https://drive.google.com/a/payu.in/file/d/0B-2lbJ9wv91JWTdOODl4c25PWHM/view)




#### Question 5 : Is there any class in SDK for card validation?
Ans : SDK is having a class named PayuUtils. Followings are its few methods 

    //to validate card number,it also take care of length validation, luan validation
    Boolean validateCardNumber(String cardNumber) 


    //returns the issuer type e.g., VISA, MAST, 
    String getIssuer(String mCardNumber) 


    // takes care of card number validation using luan algorithms, does not perform length validation
    Boolean luhn(String cardNumber)



    // takes care of cvv validation, e.g., AMEX -> 4 digit, SMAE -> no cvv, others -> 3 digits
    validateCvv(String cardNumber, String cvv)



    // to validate expiry date 
    boolean validateExpiry(int expiryMonth, int expiryYear)

To use these methods you need object of PayuUtils class.


#### Question 6 : Does merchant need PCI/DSS certificate?
Ans : When merchant collects the customer card details on their own website/server and post them to PayU.The merchant must be PCI-DSS certified in this case. For further information on PCI-DSS certification please contact your Account Manager at PayU.





#### Question 7 : Why does SSL error come in case of SRUL/FRUL
Ans : If SURL/FURL is not https then SSL error occurs but it happens some time not always with case of http.



#### Question 8 : How the pg success be affected by One Tab functionality ?
Ans : One tap payment is providing benefits in various ways:

* low bounce rates
* Reduced cart abandonment ratio
* Less failure points
* Reduced human intervention, thus ensuring low failure due to customer errors (typos etc)
Network errors

All these, clubbed together are going to improve the success rates of the PG considerably. The exact % will be known once it is widely used in the market, however since we observed with historical data that there are about 20-25% failure rates are due to above mentioned reasons, we are bound to reduce it.



#### Question 9 : Can merchant generate hash from PayU SDK ?
Ans : Yes, merchant can also generate hash from PayU SDK but it's not recommendable  because in this case key and salt will be hard coded so any body can use key and salt for making payment and it's not secure.



#### Question 10 : Is there a tool to test hash generation?
Ans : Refer [Hash generation Tool](https://payuhashgeneration.herokuapp.com/). 

The merchant can check if he is calculating hash correctly by entering the same parameters in above tool and then test the hashes generated with the hashes he is getting from his code.



#### Question 11 : Why do we need to use surl and furl ?
Ans : When the transaction successes then the PayU posts the response to the Surl and if transaction get fail the PayU posts the response to the Furl provided in post params while making payment request.

Please refer [Server Side Document](https://github.com/payu-intrepos/Documentations/wiki/4.-Server-Side) for more details.




#### Question 12 : What is the session time-out period for the PayU gateway? That is,if I do not enter any details,confirm or cancel payment for a significant amount of time,will I get a session expired message?If yes, what is that time period and is it configurable?
Ans : PayU page will not get expire, however, the PayU id generated will get expired or bounced if customer doesn't do anything after landing on the payment page for 3 hours. Also, if after 3 hours if customer enters the card details, new PayUid will get generated and transaction will go through.



#### Question 13 : How to extract the payment failure reason from Android SDK?Is there any API which can provide failure reason for a particular transaction?-
Ans : There is no API which provide failure reason for a particular transaction.To see reason for transaction failure,use your merchant panel.




#### Question 14 : Does PayU accepts the following card types :

- Master
- Visa
- Maestro(both 19 & 16 Digit)
- Rupay
- Amex 

Ans : Yes.

#### Question 15 : What is max character length for card number?

Ans : For Maestro 19, 16 otherwise.

#### Question 16 : What is max character length for CVV?
Ans :  4 is for Amex card holders, 3 otherwise.


