*  Download iOS-CustomBrowserkit [from the link here.](/payu-intrepos/iOS-Custom-Browser/releases/tag/v4.3)

*  Drag and drop the extracted content (two items - a folder and a static lib) into your project

*  Add **libz.dylib** libraries into your project

*  Add **SystemConfiguration** framework into your project

*   Import a main CB header file on top of PayUUIPaymentUIWebViewController.h
                 
        #import "PayU_CB_SDK.h"

*  Identify UIWebView implementation file (where implemented the logic for populating UIWebView during payment e.g. PayUUIPaymentUIWebViewController.m)

*   Make sure you have confirmed UIWebViewDelegate & PayUCBWebViewResponseDelegate protocols in this file

             @interface PayUUIPaymentUIWebViewController () <UIWebViewDelegate, PayUCBWebViewResponseDelegate>
       
*   Create a property of type CBConnection

        @property (strong, nonatomic) CBConnection *CBC;

*   Implement the code at the end of viewDidLoad method as follows:

         _CBC = [[CBConnection alloc]init:self.view webView:_paymentWebView];
        // _paymentWebView is an instance of UIWebView

        _CBC.isWKWebView = NO; 

        _CBC.cbServerID = CB_ENVIRONMENT_PRODUCTION;
        _CBC.analyticsServerID = CB_ENVIRONMENT_PRODUCTION;
        // CB_ENVIRONMENT_MOBILETEST pointing to MobileTest Server
        // CB_ENVIRONMENT_PRODUCTION pointing to Production Server

        // Please provide the transaction ID and Key 
        _CBC.txnID = @"abcd1234";
        _CBC.merchantKey = @"0MQaQP";
        _CBC.isAutoOTPSelect = YES;
        _CBC.cbWebViewResponseDelegate = self;

        // To disable MagicRetry
        _CBC.isMagicRetry = NO; // by default it is YES i.e. Magic Retry is enabled

        // It is optional to show PayU activity loader 
        [_CBC payUActivityIndicator];
        [_CBC initialSetup];
 
*   Implement the code at the start of UIWebViewDelegate methods as follows:

         - (void)webViewDidFinishLoad:(UIWebView *)webView;
         {    
             [_CBC payUwebViewDidFinishLoad:webView];
         }

         - (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
         {
             [_CBC payUwebView:webView didFailLoadWithError:error];
         }

         -(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:                  (UIWebViewNavigationType)naavigationType 
         {    
             [_CBC payUwebView:webView shouldStartLoadWithRequest:request];
    
             return true;
         }
          

*    Add -ObjC in Other Linker Flags in Project Build Settings.
*    Implement these methods to get success and failure callbacks from CB
    
            -(void)PayUSuccessResponse:(id)response{ } // success of txn
	        -(void)PayUFailureResponse:(id)response{ }  // failure of txn 
	        -(void)PayUConnectionError:(id)notification{ }  // on network error

*   Now compile and run your project. You will be able to see Custom Browser.

**IMPORTANT**

1. Following piece of code has to be added in AppDelegate.m file in order to make sure that Custom Browser and iOS Keyboard do not conflict with screen space when home button is pressed OR app goes in background. 

    **File Name:** AppDelegate.m

        - (void)applicationDidEnterBackground:(UIApplication *)application {
          [self.window endEditing:YES];
        }
   