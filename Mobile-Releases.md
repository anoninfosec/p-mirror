### Android SDK and Sample App 

* [Android-SDK][a-sdk]
* [Android-SDK-Sample-App][a-sample-sdk]

### iOS SDK and Sample App

* [iOS SDK][ios-sdk]
* [iOS-SDK-Sample-App][ios-sample]
* [iOS-App-WebView][ios-web]

### Android Custom Browser and Magic Retry

* [Android-Custom-Browser][a-cb]
* [Android-Sample-App-CB-Without-SDK][a-sample-cb]
* [Android-Magic-Retry][a-mr]
 
### Documentation

* [Core PayU APIs: PayU Integration Document Version 2.5.pdf][api-d]
* [SDK, CB & Magic retry Documentation: PayU Wiki][sdk-cb-d]

### YouTube Channel

* [Android](https://www.youtube.com/channel/UC3JyjFVsvSRd3MgnV-qhuBA)


   [a-sdk]: <https://github.com/payu-intrepos/Android-SDK/releases>
   [a-sample-sdk]: <https://github.com/payu-intrepos/Android-SDK-Sample-App/releases/>
   [a-cb]: <https://github.com/payu-intrepos/Android-Custom-Browser/releases> 
   [a-sample-cb]: <https://github.com/payu-intrepos/Android-SDK-Sample-App/tree/feature/CBWithoutSDK>
   [a-mr]: <https://github.com/payu-intrepos/magicretry/releases>
   [ios-sdk]: <https://github.com/payu-intrepos/iOS-SDK/releases>
   [ios-sample]: <https://github.com/payu-intrepos/iOS-SDK-Sample-App/releases>
   [ios-web]: <https://github.com/payu-intrepos/iOS-App-WebView-PG/releases>
   [scr]: <https://drive.google.com/a/payu.in/folderview?id=0B4URmsDLhGXmfjFmbDQ5b2V0bVhjdTZMNExMVHRFMG1PRFFYeUV0LU9nSWU4U0pqaW00OU0&usp=drive_web&ddrp=1#>
   [api-d]: <https://drive.google.com/file/d/0B-ToBwKkXbZuNEdEREtlRnFBWHM/view?usp=sharing>
   [sdk-cb-d]: <https://github.com/payu-intrepos/Documentations/wiki/1.-Getting-Started>
   