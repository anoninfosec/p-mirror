#  Webservices Integration Doc - iOS

To call any of the below API you need to create an object of class `PayUWebServiceResponse` and call the respective methods, you will get the result in completion handler of the method. If there is any error in the parameters passed by merchant then it will give the error in `errorMessage` string else you will get the parsed object.

    PayUWebServiceResponse *webServiceResponse = [PayUWebServiceResponse new];

### 2.1 payment_related_details_for_mobile_sdk
This API is helpful in getting stored card and available Netbanking.

To integrate this API call the method `getPayUPaymentRelatedDetailForMobileSDK` as below:

    [webServiceResponse getPayUPaymentRelatedDetailForMobileSDK:self.paymentParamForPassing withCompletionBlock:^(PayUModelPaymentRelatedDetail *paymentRelatedDetails, NSString *errorMessage, id extraParam) {
    
    if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
    }else{
        // It is good to go & paymentRelatedDetails  is having the full detail of it
    }
    }];

    Note: paymentRelatedDetails.availablePaymentOptionsArray gives you all the available payment options for you.

### 2.2 VAS
This API is used to get the list of down NetBanking and down cardbin

To integrate this API call the method `callVASForMobileSDKWithPaymentParam` as below

    [webServiceResponse callVASForMobileSDKWithPaymentParam:self.paymentParamForPassing];

Now to check whether the particular NetBanking is down or not just pass the bankCode or cardbin (first 6 digit of cardnumber) to it and in `completionBlock` you will get the response as below

    [webServiceResponse getVASStatusForCardBinOrBankCode:@"AXIB" withCompletionBlock:^(id ResponseMessage, NSString *errorMessage, id extraParam) {
        	if (errorMessage == nil) {
            		if (ResponseMessage == nil) {
    			        // It means given NetBanking code or cardnumber is not down i.e. it is in good to go condition
            		}else{
                		NSString * responseMessage = [NSString new];
                		responseMessage = (NSString *) ResponseMessage;
    			        // It means given NetBanking code or cardnumber is down and you can display the responseMessage if you want or you can customize it
            		}
        	}else{
    		    // Something went wrong errorMessage is having the Detail
        	}
    	}];

### 2.3 Delete Store Card
This API is helpful in Deleting StoredCard.

To integrate this API call the method `deleteStoredCard` as below

    [webServiceResponse deleteStoredCard:self.paymentParamForPassing withCompletionBlock:^(NSString *deleteStoredCardStatus, NSString *deleteStoredCardMessage, NSString *errorMessage, id extraParam) {
                if (errorMessage) {
            // Something went wrong errorMessage is having the Detail
                }
                else{
            //It is good to go & deleteStoredCardMessage is having the full detail of it
                }
            }];
### 2.4 Get Offer Status
This API is helpful in checking whether offer is available for given cardNumber or not and if available then it will return the discount amount.

To integrate this API call the method `getOfferStatus` as below

    [webServiceResponse getOfferStatus:self.paymentParamForPassing withCompletionBlock:^(PayUModelOfferStatus *offerStatus, NSString *errorMessage, id extraParam) {
        if (errorMessage == nil) {
            //It is good to go & offerStatus.discount contains the discounted amount if there is any offer & offerStatus.msg contains the message why offer is not available
        }
        else{
            // Something went wrong errorMessage is having the Detail
        }
    }];

### 2.5 CheckOfferDetails:
This API is helpful in getting details of offerkey, whether the given offer key is valid or not. If we provide the amount to this API then this API gives the discount value also.
We can use this API for payment mode such as CC/DC, StoreCard, OneTap Card and NetBanking
To integrate this API call the method getOfferDetails

##### 2.5.1 For CCDC:
Set the cardnumber in paymentParamForPassing object as below:

    self.paymentParamForPassing.cardNumber = @”5123456789012346”;

After setting cardNumber  call the method getOfferDetails by passing second parameter as PAYMENT_PG_CCDC as below:
```sh
[webServiceResponse
getOfferDetails:self.paymentParamForPassing forPaymentType:PAYMENT_PG_CCDC withCompletionBlock:^(PayUModelOfferDetails *offerDetails, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // offerDetails object is having the detail of the offer key
      }
    }];
```
##### 2.5.2 For StoredCard/OneTapCard:

If merchant want to check offer for one particular storedcard then set the cardtoken as below:

    self.paymentParamForPassing.cardToken = @”643567vbhbvgh5678gvv77b”;

If merchant want to check offer for all storedcard then set cardToken as empty or nil:
```sh
    self.paymentParamForPassing.cardToken = nil;
```
Note: For StoredCard/OneTapCard, you must set userCredentials inside your payment params
After setting cardToken call the method getOfferDetails by passing second parameter as PAYMENT_PG_STOREDCARD as below:
```sh
[webServiceResponse
getOfferDetails:self.paymentParamForPassing forPaymentType:PAYMENT_PG_STOREDCARD withCompletionBlock:^(PayUModelOfferDetails *offerDetails, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // offerDetails object is having the detail of the offer key
      }
    }];    
```
##### 2.5.3 For NetBanking:
In case merchants want to know whether any particular Net Banking is eligible for offer then set the bankcode as below:
```sh
    self.paymentParamForPassing.bankCode = @”AXIB”;
```
In case Merchant wants to know the list of net bankings which are eligible for any particular Offer then set bankCode as nil or empty as below:
```sh
    self.paymentParamForPassing.bankCode = nil;
```
After setting bankcode call the method getOfferDetails by passing second parameter as PAYMENT_PG_NET_BANKING as below:
```sh
[webServiceResponse
getOfferDetails:self.paymentParamForPassing forPaymentType:PAYMENT_PG_NET_BANKING withCompletionBlock:^(PayUModelOfferDetails *offerDetails, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // offerDetails object is having the detail of the offer key
      }
    }];
```

### 2.6 GetEMIAmountAccordingToInterest:
This API is helpful in getting details of all the available EMI’s. For this API, you need to set amount in the payment params as below:
```sh
    self.paymentParamForPassing.amount = @"100";
```
To integrate this API call the method getEMIAmountAccordingToInterest as below
```sh
[webServiceResponse
getEMIAmountAccordingToInterest:self.paymentParamForPassing withCompletionBlock:^(NSDictionary *dictEMIDetails, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // dictEMIDetails is having the EMI detail
      }
    }];
```
### 2.7 Get User Cards:
This API is helpful in getting all the Stored Card for particular user. For this API, you need to set userCredentials in the payment params as below:
```sh
    self.paymentParamForPassing.userCredentials = @"gtKFFx:UmangArya";
```
To integrate this API call the method getUserCards as below
```sh
[webServiceResponse
getUserCards:self.paymentParamForPassing withCompletionBlock:^(NSDictionary *dictStoredCard, NSString *errorMessage, id extraParam){
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // dictStoredCard is having all the storedcard information
      }
    }];
```
### 2.8 Verify Payment:
This web-service is used to reconcile the transaction with PayU. When we post back the final response to you (merchant), we provide a list of parameters (including the status of the transaction – For example, success, failed etc). On a few occasions, the transaction response is initiated from our end, but it doesn’t reach you due to network issues or user activity (like refreshing the browser etc).
We strongly recommend that this API is used to reconcile with PayU’s database once you receive the response. This will protect you from any tampering by the user and help in ensuring safe and secure transaction experience.
For this API, you need to set transactionID inside your payment params as below:
```sh
    self.paymentParamForPassing.transactionID = @"tnxID1|txnID2|txnID3";
```
Multiple txnID can be sent by using pipe Symbol(|) as seprator. 
To integrate this API call the method verifyPayment as below
```sh
[webServiceResponse
verifyPayment:self.paymentParamForPassing withCompletionBlock:^(NSDictionary *dictVerifyPayment, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // dictVerifyPayment is having the verifyPayment detail
      }
    }];
```
### 2.9 Edit User Card:
This API is used to edit the details of an existing stored card in the vault. Only Card Name and Name on card can be edited. For this API, you need to set following parameter in the payment params as below:
```sh
self.paymentParamForPassing.userCredentials = @"gtKFFx:UmangArya";
    self.paymentParamForPassing.cardToken = @"745d72e2fd9b7e88824fef4e7ed7dac1f";
    self.paymentParamForPassing.cardName = @"UmangHDFC";
    self.paymentParamForPassing.cardMode = @"CC";
    self.paymentParamForPassing.cardType = @"MAST";
    self.paymentParamForPassing.nameOnCard = @"testHDFC";
    self.paymentParamForPassing.cardNo = @"5123456789012346";
    self.paymentParamForPassing.expiryMonth = @"11";
    self.paymentParamForPassing.expiryYear = @"2019";
```
To integrate this API call the method editUserCard as below
```sh
[webServiceResponse
editUserCard:self.paymentParamForPassing withCompletionBlock:^(PayUModelStoredCard *objStoredCard, NSString *errorMessage, id extraParam){
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // objStoredCard object is having the updated stored card parameters
      }
    }];
```
### 2.10 Delete One Tap Token:
This API is helpful in deleting one tap token for a given user card. For this API, you need to set userCredential and cardToken in the payment params as below:
```sh
    self.paymentParamForPassing.userCredentials = @"gtKFFx:UmangArya";
    self.paymentParamForPassing.cardToken = @"745d72e2fd9b7e88824fef4e7ed7dac1f";
```
To integrate this API call the method deleteOneTapToken as below
```sh
[webServiceResponse
deleteOneTapToken:self.paymentParamForPassing withCompletionBlock:^(NSString *deleteOneTapTokenMsg ,NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // deleteOneTapTokenMsg is having response Msg given by PayU server after successful deletion of cardToken
      }
    }];
```
### 2.11 Check Is Domestic:
This API is used to detect whether a particular bin number is international or domestic. It is also useful to determine the card’s issuing bank, the card type brand – i.e, Visa, Master etc and also the Card Category – i.e. Credit/Debit etc. Bin number is the first 6 digits of a Credit/Debit card. For this API, you need to set cardNumber in the payment params as below:
```sh
self.paymentParamForPassing.cardNumber = @"512345";
```
To integrate this API call the method checkIsDomestic as below
```sh
[webServiceResponse
checkIsDomestic:self.paymentParamForPassing withCompletionBlock:^(PayUModelCheckIsDomestic *checkIsDomestic, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // checkIsDomestic object is having the required detail
      }
    }];
```
### 2.12 Get Transaction Info:
This API is used to extract the transaction details between two given time periods. The API takes the input as two dates along with time (initial and final), between which the transaction details are needed. The output would consist of the status of the API (success or failed) and all the transaction details in an array format. For this API, you need to set startTime and endTime in the payment params as below:
```sh
self.paymentParamForPassing.startTime = @"2014-01-12 16:00:00";
self.paymentParamForPassing.endTime = @"2014-01-12 16:00:50";
```
To integrate this API call the method getTransactionInfo as below
```sh
[webServiceResponse
getTransactionInfo:self.paymentParamForPassing withCompletionBlock:^(NSArray *arrOfGetTxnInfo, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // arrOfGetTxnInfo is the array of PayUModelGetTxnInfo which is having detail of transaction
      }
    }];
```
### 2.13 Save User Card:
This API is used for saving a card to the vault. For this API, you need to set following parameter in the payment params as below:
```sh
 self.paymentParamForPassing.userCredentials = @"gtKFFx:UmangArya";
    self.paymentParamForPassing.cardName = @"UmangHDFC";
    self.paymentParamForPassing.cardMode = @"CC";
    self.paymentParamForPassing.cardType = @"MAST";
    self.paymentParamForPassing.nameOnCard = @"testHDFC";
    self.paymentParamForPassing.cardNo = @"5123456789012346";
    self.paymentParamForPassing.expiryMonth = @"11";
    self.paymentParamForPassing.expiryYear = @"2019";
    self.paymentParamForPassing.duplicateCheck = @"1";// duplicateCheck = 1 means in response you will get duplicate count otherwise not

```
To integrate this API call the method saveUserCard as below:
```sh
[webServiceResponse
saveUserCard:self.paymentParamForPassing withCompletionBlock:^(PayUModelStoredCard *objStoredCard, NSString *errorMessage, id extraParam) {
      if (errorMessage) {
        // Something went wrong errorMessage is having the Detail
      }
      else{
        // objStoredCard is having the newly save card details
        // objStoredCard.duplicateCardCount gives the number of duplicate card found for that merchant for this card
      }
    }];
```