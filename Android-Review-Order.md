<b>Review Order<b>

Review order is a feature of CB by which merchant can display an ongoing transaction’s information to the user on bank page. This will help the user to verify transaction related critical information while making a payment, and prevents users from pressing back button, cancelling the transaction, just to review or re-check their order details.

<b>Prerequisite:<b>

To integrate Review Order (RO), merchant first need to integrate CB via Simplified integration in their app. Documentation for which can be found here.

<b>Integration options<b>

You have two options by which transaction information can be displayed using RO:

<ol><li><b>With Default PayU’s UI:</b> You pass the data to be displayed. We create a view with your information. We control the UI and places at which info should be displayed.</li>
<li><b>With Custom UI:</b> You pass us an object of view(layout). You design the view and fill information in it. We display the view as is in the Review Order screen of CB.</li></ol>

<b>Detailed integration steps </b>

<ol><li><b>With PayU’s UI:</b></li>

Make object of "ReviewOrderBundle"


```sh
ReviewOrderBundle reviewOrderBundle = new ReviewOrderBundle();
```

Add payment details to bundle in form of key value pair

e.g.
```sh
reviewOrderBundle.addOrderDetails("Amount","10.00");
reviewOrderBundle.addOrderDetails("Mobile","1111111111");
reviewOrderBundle.addOrderDetails("Email","email@testsdk1.com");
```

setReviewOrderDefaultViewData : Set bundle data to Review Order "setReviewOrderDefaultViewData" method

```sh
customBrowserConfig.setReviewOrderDefaultViewData(reviewOrderBundle);
```

<li><b>With Custom UI:</b></li>

Create a layout as per your design which contain the data you want to display. Use mention below method to set the layout information.

<b>setReviewOrderCustomView:</b> Set your custom view to review order
example : 

```sh
customBrowserConfig.setReviewOrderCustomView(R.layout.review_custom_layout);
```

<b>Customizations</b>

<i>setEnableReviewOrder</i> - Enable review order

Input params are : 

CustomBrowserConfig.ENABLE 
CustomBrowserConfig.DISABLE

example : 

```sh
customBrowserConfig.setEnableReviewOrder(CustomBrowserConfig.ENABLE);
```

 > Note: Default value is CustomBrowserConfig.DISABLE

<i>setReviewOrderButtonText</i> : To change review order button Text name
example: 

```sh
customBrowserConfig.setReviewOrderButtonText(“test1234”);
```

 > Note: Maximum length of text is 16 characters

Please write at <i><b>mobile-integration@payu.in</b></i> for further queries.







