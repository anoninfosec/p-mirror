#Server Side

***

**IMPORTANT:** PayU Android SDK version3.0 is deprecated.We are no longer supporting it.

##Generate Hash
* What is Hash?  
 Every transaction (payment or non-payment) needs a hash by the merchant before sending the transaction details to PayU. This is required for PayU to validate the authenticity of the transaction. This should be done on your server.  

* Generating Hash
 * ￼Payment Hash
        
          sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt)

 * Webservice Hash - sample

          sha512(key|command|var1|salt) 

 > Note: Please refer the [web integration document](https://drive.google.com/file/d/0B-ToBwKkXbZuNEdEREtlRnFBWHM/view?usp=sharing) for more details  

 * Some pre-programmed scripts to generate hash.
       
        * For latest Android PayU SDK Ver4+ and iOS PayU SDK v3.0 onwards
            * [JAVA Hash Generation Code](https://drive.google.com/file/d/0B-ToBwKkXbZuRjlES0p2cVVQZFk/view?usp=sharing)
            * [PHP Hash Generation Code](https://drive.google.com/file/d/0B-ToBwKkXbZuX2Y5TmppNUZnVWM/view?usp=sharing)

##Return Url - (SURL / FURL)  
* Return URL is where PayU redirects the user after the transaction is completed. PayU sends the
data related to transactions while redirecting so that you can check the status of the transaction.  

* How to create surl/furl page?  
 The surl/furl page is hosted on your server to communicate back to client application when the transaction is completed. You may check the status of the transaction and take actions accordingly. Inside mobile applications, it is important that the user is redirected back to app whenever a transaction is completed. After the transaction is complete, Payu posts the response to the surl / furl.  

* Sample code (For Android):  
 * [JAVA(.jsp)](https://drive.google.com/file/d/0B-ToBwKkXbZuMkh0VWhlUFNjd2c/view)
 * [PHP](https://drive.google.com/file/d/0Bzp7lKDOPgKba0JfTHl4R201bXM/view)        

* Sample code (For iOS):  
 * [JAVA(.jsp)](https://drive.google.com/file/d/0B-ToBwKkXbZuUHBzX0E2U0h6eFU/view)
 * [PHP](https://drive.google.com/file/d/0B-ToBwKkXbZuM2VPT01kRG16c1U/view)        


* Some links for reference (Notice the difference in scripts for Android and iOS)
 * android surl and furl  
https://payu.herokuapp.com/success  
https://payu.herokuapp.com/failure  

 * iOS surl and furl  
https://payu.herokuapp.com/ios_success    
https://payu.herokuapp.com/ios_failure



##Response (comes with surl/furl)

Sample success response  

    Array
    (
        [mihpayid] => 316717697
        [mode] => CC
        [status] => success
        [unmappedstatus] => captured
        [key] => smsplus
        [txnid] => 74e9a70d171df41f7c6a
        [amount] => 2.00
        [cardCategory] => domestic
        [discount] => 0.00
        [net_amount_debit] => 2
        [addedon] => 2015-04-13 18:10:58
        [productinfo] => Product Info
        [firstname] => Payu-Admin
        [lastname] => 
        [address1] => 
        [address2] => 
        [city] => 
        [state] => 
        [country] => 
        [zipcode] => 
        [email] => test@example.com
        [phone] => 1234567890
        [udf1] => 
        [udf2] => 
        [udf3] => 
        [udf4] => 
        [udf5] => 
        [udf6] => 
        [udf7] => 
        [udf8] => 
        [udf9] => 
        [udf10] => 
        [hash] => 0e92de4a135724da69011f0b39093c20431fe07d1f17f6ca3baa7fdcf4b0e5af333e4fb49a52544e65a110dfa4db2f27cff5d587bdafa67ef3baafc2f8928e46
        [field1] => 510370167829
        [field2] => 047751
        [field3] => 4602057101851030
        [field4] => 4602057101851030
        [field5] => 
        [field6] => 
        [field7] => 
        [field8] => 
        [field9] => SUCCESS
        [payment_source] => payu
        [PG_TYPE] => HDFCPG
        [bank_ref_num] => 4602057101851030
        [bankcode] => CC
        [error] => E000
        [error_Message] => No Error
        [name_on_card] => benjamin franklin
        [cardnum] => 438628XXXXXX2452
        [cardhash] => This field is no longer supported in postback params.
    }  
> Note: The possible error codes and messages [**Transaction Error Code.pdf**](https://drive.google.com/open?id=0B-2lbJ9wv91JTHdsWUhLSnpHNVU)

##Post-Transaction hash sequence:( Mandatory ) 

Merchant needs to form the below hash sequence and verify it with the hash sent by PayU in the Post Response(without additional charges): 
 
    sha512(SALT|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key) 


Merchant needs to form the below hash sequence and verify it with the hash sent by PayU in the Post Response(with additional charges): 

    sha512(additionalCharges|SALT|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key) 

Where, additionalCharges value must be same as the value Posted from PayU to the merchant in the response.

**IMPORTANT:** This hash value must be compared with the hash value posted by PayU to the merchant. If both match, then only the order should be processed. **If they don’t match, then the transaction has been tampered with by the user and hence should not be processed further.**    
 
**If you do not calculate reverse hash from your end, the transaction can be tampered or hacked.**


* Sample code For Reverse Hash Generation (iOS & Android)

 * [reverseHash.php ](https://drive.google.com/file/d/0Bzp7lKDOPgKba0JfTHl4R201bXM/view?usp=sharing)


##payment_related_details_for_mobile_sdk API


This API is used to get all payment option and its URLs are :
 
Test URL : ``` https://mobiletest.payu.in/merchant/postservice?form=2 ```

Production URL : ``` https://info.payu.in/merchant/postservice.php?form=2 ```

To call any PayU API, you need to generate hash from your server using following pattern :
 
 ```sh
sha512(key|command|var1|salt)
```

```sh
  key=YOUR KEY
  command=payment_related_details_for_mobile_sdk
  salt= YOUR SALT
  var1= default(if you want stored cards use var1 as user_credentilas else default)
```
for [user_credentilas](https://drive.google.com/file/d/0B-ToBwKkXbZuNEdEREtlRnFBWHM/view) see p53

 * To call API, you need to pass following parameters with test/prod URLs :

```sh
key=gtKFFx(YOUR KEY)
hash=e43ede14c88c70e27f47001b4207a......(generated using sha512(key|command|var1|salt) from your server)
command=payment_related_details_for_mobile_sdk
var1= default(if you want stored cards use var1 as user_credentilas else default)

```
 > Note: Please refer the [web integration document](https://drive.google.com/file/d/0B-ToBwKkXbZuNEdEREtlRnFBWHM/view?usp=sharing) for more details  


##Updates In Surl-Furl Implementation. 
**NOTE :** This updation will only work for [latest SDK and CB](https://github.com/payu-intrepos/Documentations/wiki/Mobile-Releases)

Surl(success url), Furl(failure url) are two urls (https POST) should be given by merchant as post param while making payment.  As soon as the transaction completes payu post the data back to surl/furl depends on the transaction status. 

######Current implementation

SURL/FURL - should implement a javascript interface function named 

	PayU.onSuccess(“data”) or PayU.onFailure(“data”); 

example 


	<html>
	<body>
	<script>
		var data = “Data to send back to app from surl/furl”;
		PayU.onSuccess(data); // in case of success (surl);
		PayU.onFailure(data); // in case of failure (furl);
	</script>
	</body>
	</html>

As soon as this surl/furl makes interface call to sdk/custombrowesr, you can get the data from 

	data.getStringExtra("result")

######New implementation. (Optional)

SURL / FURL - Javascript interface is no longer mandatory. - lets say if a merchant wants to send custom data to app from surl/furl, then he can make use of the PayU.onSuccess() or PayU.onFailure() function. 

After the transaction in app’s onActinvityResult function 

For Return data from Surl/furl (if any) 

	data.getStringExtra("result")

- For PayU’s Default data (recommended way)


		@Override
		   protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
	       if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
           if(data != null ) {

		String merchantData = data.getStringExtra("result"); // Data received from surl/furl
		String payuData = data.getStringExtra("payu_response"); // Response received from payu
              }
		}
		}


- For Storing merchant’s hash in One click payment mode. 


	
		@Override
   
		protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
		if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
           if(data != null ) {

               try {
                   JSONObject jsonObject = new JSONObject(data.getStringExtra(PayuConstants.PAYU_RESPONSE));

                   if(storeOneClickHash == PayuConstants.STORE_ONE_CLICK_HASH_SERVER && jsonObject.has(PayuConstants.CARD_TOKEN) && jsonObject.has(PayuConstants.MERCHANT_HASH)){ // we have merchant hash, lets store it merchant server.
                       storeMerchantHash(jsonObject.getString(PayuConstants.CARD_TOKEN), jsonObject.getString(PayuConstants.MERCHANT_HASH));
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               } catch (Exception e){

               }
           }else{
               Toast.makeText(this, "Could not receive data", Toast.LENGTH_LONG).show();
           }
        }
       
        }


	
		private void storeMerchantHash(String cardToken, String merchantHash){

            final String postParams = "merchant_key="+key+"&user_credentials="+var1+"&card_token="+cardToken+"&merchant_hash="+merchantHash;

               new AsyncTask<Void, Void, Void>(){

                   @Override
                   protected Void doInBackground(Void... params) {
                       try {
                           //  https://mobiledev.payu.in/admin/wis.php?action=add&uid=124&mid=457&token=74588&cvvhash=0123456789031

                           URL url = new URL("https://payu.herokuapp.com/store_merchant_hash");

                           byte[] postParamsByte = postParams.getBytes("UTF-8");

                           HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                           conn.setRequestMethod("POST");
                           conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                           conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                           conn.setDoOutput(true);
                           conn.getOutputStream().write(postParamsByte);

                           InputStream responseInputStream = conn.getInputStream();
                           StringBuffer responseStringBuffer = new StringBuffer();
                           byte[] byteContainer = new byte[1024];
                           for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                               responseStringBuffer.append(new String(byteContainer, 0, i));
                           }

                           JSONObject response = new JSONObject(responseStringBuffer.toString());


                       } catch (JSONException e) {
                           e.printStackTrace();
                       } catch (MalformedURLException e) {
                           e.printStackTrace();
                       } catch (ProtocolException e) {
                           e.printStackTrace();
                       } catch (UnsupportedEncodingException e) {
                           e.printStackTrace();
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                       return null;
                   }

                   @Override
                   protected void onPostExecute(Void aVoid) {
                       super.onPostExecute(aVoid);
                       this.cancel(true);
                   }
               }.execute();
         }