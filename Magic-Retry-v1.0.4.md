
## MagicRetry Integration With Custom Browser (5.2.4+)

### 1. Add following dependency in build.gradle

```sh
dependencies{
    compile 'com.payu.magicretry:magicretry:1.0.4'
}
```
### 2.  Changes in PaymentsActivity.java

* PaymentsActivity should implement MagicRetryFragment.ActivityCallback

```sh
PaymentsActivity extends AppCompatActivity implements MagicRetryFragment.ActivityCallback
```

* Add following field to PaymentsActivity.class

```sh
MagicRetryFragment  magicRetryFragment;
```

* Add Following functions in PaymentsActivity class

```sh
private void initMagicRetry() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        magicRetryFragment = new MagicRetryFragment();
        Bundle newInformationBundle = new Bundle();
        newInformationBundle.putString(MagicRetryFragment.KEY_TXNID, txnId);
        magicRetryFragment.setArguments(newInformationBundle);

        Map<String, String> urlList = new HashMap<String, String>();
        urlList.put(url, payuConfig.getData());
        magicRetryFragment.setUrlListWithPostData(urlList);

        fragmentManager.beginTransaction().add(R.id.magic_retry_container, magicRetryFragment, "magicRetry").commit();
        // magicRetryFragment = (MagicRetryFragment) fragmentManager.findFragmentBy(R.id.magicretry_fragment);

        toggleFragmentVisibility(Util.HIDE_FRAGMENT);

        magicRetryFragment.isWhiteListingEnabled(true);
    }


    public void toggleFragmentVisibility(int flag){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (!isFinishing()) {
            if (flag == Util.SHOW_FRAGMENT) {
                // Show fragment
                ft.show(magicRetryFragment).commitAllowingStateLoss();
            } else if (flag == Util.HIDE_FRAGMENT) {
                // Hide fragment
                ft.hide(magicRetryFragment).commitAllowingStateLoss();
                // ft.hide(magicRetryFragment);
                Log.v("#### PAYU", "hidhing magic retry");
            }
        }
    }
    @Override
    public void showMagicRetry() {
        toggleFragmentVisibility(Util.SHOW_FRAGMENT);
    }

    @Override
    public void hideMagicRetry() {
        toggleFragmentVisibility(Util.HIDE_FRAGMENT);
    }
```

* Add Following code in onCreate() in PaymentsActivity class
```sh
// Before setting Webview's setWebViewClient
	   initMagicRetry();

// change mWebView.setWebViewClient(new PayUWebViewClient(bank)); to:

           mWebView.setWebViewClient(new PayUWebViewClient(bank, magicRetryFragment,merchantKey));


// After setting Webview's setWebViewClient
	   //mWebView is the WebView Object
	    magicRetryFragment.setWebView(mWebView);
	   // MR Integration - initMRSettingsFromSharedPreference
	   magicRetryFragment.initMRSettingsFromSharedPreference(this);

```

### 3. Changes in layout file ( activity_payments.xml)

Add following layout code after the Webview layout.
>Note - Both WebView and MagicRetry layout should be inside framelayout

```sh
<LinearLayout
        android:layout_centerInParent="true"
        android:orientation="vertical"
        android:id="@+id/magic_retry_container"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
</LinearLayout>
```